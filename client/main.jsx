import React from 'react';
import { render } from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Tasks } from '../imports/api/collections.js';
import '../imports/startup/accounts-config.js';
import '../imports/client/routes';

import App from '../imports/ui/App.jsx';
 
Meteor.startup(() => {
  render(<App />, document.getElementById('react-root'));
});