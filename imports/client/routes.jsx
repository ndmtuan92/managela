import React  from 'react';
import { mount } from 'react-mounter';

//components
import App from '../ui/App';

import Home from '../ui/pages/Home';
import BoardLayout from '../ui/pages/BoardLayout';

FlowRouter.route( '/', {
  name: 'Home',
  action( params ) {
    mount(App, {
      main: <Home/>
    })
  }
});

FlowRouter.route( '/board/:id', {
  name: 'BoardLayout',
  action( params ) {
    mount(App, {
      main: <BoardLayout/>
    })
  }
});
