import React, { Component } from 'react';
import AccountsUIWrapper from './AccountsUIWrapper.jsx';

export default class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>                        
              </button>
              <a className="navbar-brand" href="/"><i className="fa fa-home fa-fw"></i> Home</a>
            </div>
            <div id="myNavbar">
              <ul className="nav navbar-nav">
                <li>
                  <AccountsUIWrapper />
                </li>
              </ul>

              <div className="col-sm-3 col-md-3">
                <form className="navbar-form" role="search">
                <div className="input-group">
                    <input style={{height:'34px'}} type="text" className="form-control" placeholder="Search" name="q" />
                    <div className="input-group-btn">
                        <button className="btn btn-default" type="submit"><i className="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </nav>

        <div id="content-render">
          {this.props.main}
        </div>
      </div>
    );
  }
}