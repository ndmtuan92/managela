import React, { Component } from 'react';

// Task component - represents a single todo item
export default class Boards extends Component {
  render() {
    return (
      <div className="col-md-3">
      	<a href={'/board/' + this.props.board._id}>
	      	<div className="item-board">
	      		<h4 className="board-title">{this.props.board.title}</h4>
	      	</div>
      	</a>
      </div>
    );
  }
}
