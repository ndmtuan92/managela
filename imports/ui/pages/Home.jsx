import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { createContainer } from 'meteor/react-meteor-data';

import { Boards } from '../../api/collections.js';

import Board from '../Boards';

import { Meteor } from 'meteor/meteor';

class Home extends Component{
	handleSubmit(event) {
    event.preventDefault();
 
    // Find the text field via the React ref
    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
 
    Boards.insert({
      title: text,
      createdAt: new Date(), // current time
    });
 
    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }

  renderBoards() {
    return this.props.boards.map((board) => (
      <Board key={board._id} board={board} />
    ));
  }
  
	render(){
		return(
			<div className="container">
				<div className="row">
			    {/*item board*/}
			    <div className="col-md-3">
			      <div className="item-board item-board-add">
			        <form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
			          <div className="form-group">
			            <label htmlFor="add-board"><i className="fa fa-plus fa-fw"></i> Add new board:</label>
			            <input
			            	required
			              id="add-board"
			              type="text"
			              ref="textInput"
			              placeholder="Type to add new board"
			              className="form-control"
			            />
			          </div>
			        </form>
			      </div>
			    </div>
			    {this.renderBoards()}
			  </div>
		  </div>
		)
	}
}

export default createContainer( () => {
	return {
    boards: Boards.find({}, { sort: { createdAt: -1 } }).fetch(),
    currentUser: Meteor.user()
  };
}, Home)