import React, { Component } from 'react';
import { EditableTextField, EditableTextArea } from 'react-bootstrap-xeditable';
import ReactDOM from 'react-dom';
import { createContainer } from 'meteor/react-meteor-data';

import { Lists, Boards, Users } from '../../api/collections.js';

import List from '../List'

import { Meteor } from 'meteor/meteor';

import Select from 'react-select';
import 'react-select/dist/react-select.css';

class BoardLayout extends Component {
	constructor(props) {
    super(props);
    this.state = {
    	removeSelected: true,
			disabled: false,
			crazy: false,
			stayOpen: false,
			rtl: false,
			data: {}
    }
    this.state = Object.assign(this.props);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  componentWillReceiveProps(newProps) {
	  if (this.state !== newProps) {
	    this.setState(newProps);
	  }
	}
	handleUpdate(name, value){
  	let data = {};
  	data[name] = value;
  	Boards.update(
    	{ _id: this.state.data._id },
    	{
    		$set : data
    	}
    )
  }
	handleSubmit(event) {
    event.preventDefault();

    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();

    Lists.insert({
      title: text,
      boardId : this.props.id,
      createdAt: new Date(),
    });
 
    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }
  handleSelectChange (members) {
		this.setState({ members: members });
		Boards.update(
			{_id: this.state.data._id},
			{
				$set : { members: members }
			}
		)
	}
	toggleRtl (e) {
		let rtl = e.target.checked;
		this.setState({ rtl });
	}
	renderList(){
		return this.props.dataList.map( (list) => (
			<List key={list._id} data={list} />
		))
	}
	render(){
		return(
			<div className="container-fluid">
				{ (this.state.data) && 

					<div className="row">
						<div className="col-md-3">
							<div className="panel panel-success">
					      <div className="panel-heading">
						      <b>
						      	<EditableTextField 
				          		name='title'
				          		value={this.state.data.title}
				          		onUpdate={this.handleUpdate} 
				          		placeholder='Please input title'
				          	/>
						      </b>
					      </div>

					      <div className="panel-body">
					      	<EditableTextArea
			          		name='description'
			          		value={this.state.data.description}
			          		onUpdate={this.handleUpdate} 
			          		placeholder='Please input title'
			          	/>

			          	<div className="clearfix"></div>
					      	<label><b>Members: </b></label>
					      	<div className="row">
					      		<div className="col-md-12">
						      		<Select
												closeOnSelect={!this.state.stayOpen}
												disabled={this.state.disabled}
												multi
												onChange={this.handleSelectChange.bind(this)}
												options={this.props.memberOptions}
												placeholder="Search members and select"
							          removeSelected={this.state.removeSelected}
												rtl={this.state.rtl}
												simpleValue
												value={this.state.data.members}
											/>
										</div>
					      	</div>

					      </div>
					    </div>
						</div>
						<div className="col-md-9">
							<div className="row well" style={{overflow: scroll, height: '100%'}}>
								{this.renderList()}
								<div className="col-md-3">
									<div className="item-list form-add-list">
					          <form className="new-list" onSubmit={this.handleSubmit.bind(this)} >
						          <div className="form-group">
						            <input
						            	required
						              id="add-list"
						              type="text"
						              ref="textInput"
						              placeholder="Type to add new list"
						              className="form-control"
						            />
						          </div>
						        </form>
									</div>
								</div>
							</div>
						</div>

					</div>
				}
			</div>
		);
	}
}
export default createContainer((props) => {
	FlowRouter.watchPathChange();
	const id = FlowRouter.current().params.id;
	const memberOptions = Meteor.users.find().fetch();
	memberOptions.map(function(member, index){
		memberOptions[index].label = member.username;
		memberOptions[index].value = member._id;
	})

  return {
  	id : id,
  	data: Boards.findOne(id),
  	dataList: Lists.find({boardId : id}).fetch(),
  	memberOptions: memberOptions,
  };
}, BoardLayout);