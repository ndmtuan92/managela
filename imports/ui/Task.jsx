import React, { Component } from 'react';
import { Lists, Tasks } from '../api/collections.js';
import { EditableTextArea, EditableTextField, EditableSelect } from 'react-bootstrap-xeditable';
import _ from 'lodash'
import Modal from 'react-modal';
const customStyles = {
  overlay : {
  	zIndex : 3,
  	backgroundColor : 'rgba(0, 0, 0, 0.75)',
  },
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    padding: 0,
    width: '500px',
  }
};
// Task component - represents a single todo item
export default class Task extends Component {
	constructor(props) {
    super(props);
    const s = {
    	modalIsOpen: false
    }
		this.state = Object.assign(s, this.props)
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  componentWillReceiveProps(newProps) {
	  if (this.state !== newProps) {
	    this.setState(newProps);
	  }
	}
	removeTask(event){
		event.preventDefault();
		Tasks.remove(this.props.data._id);
	}
  openModal(event) {
    this.setState({modalIsOpen: true});
  }
  closeModal() {
    this.setState({modalIsOpen: false});
  }
  handleUpdate(name, value){
  	let data = {};
  	data[name] = value;
  	Tasks.update(
    	{ _id: this.state.data._id },
    	{
    		$set : data
    	}
    )
  }
  render() {
    return (
    	<div>
    		<Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel="Example Modal"
          style={customStyles}
        >

		        <div className="modal-header">
		          <div className="modal-title">
		          	<EditableTextField 
		          		name='title'
		          		value={this.state.data.title}
		          		onUpdate={this.handleUpdate} 
		          		placeholder='Please input title'
		          	/>
		          	<p className="text-muted">in list <b>{this.state.parent.title}</b></p>
		          </div>
		        </div>
		        <div className="modal-body">
		        	<div className="col-md-8">
			        	<label>Assigned: <button type="button" className="btn btn-primary btn-xs"><i className="fa fa-plus"></i></button></label>
			        	<div className="row">
			        		<div className="list-members">
				        		<div className="col-md-2 col-xs-3">
				        			<img src="http://www.gravatar.com/avatar/5658ffccee7f0ebfda2b226238b1eb6e?size=34&default=mm" className="img-responsive img-circle" />
				        		</div>
			        		</div>
			        	</div>

			        	<div className="row" style={{marginTop: '10px'}}>
			        		<div className="col-md-12">
			        			<div><label>Description: </label></div>
			        			<EditableTextArea
				          		name='description'
				          		value={this.state.data.description}
				          		onUpdate={this.handleUpdate} 
				          		placeholder='Please input description'
				          	/>
			        		</div>
			        	</div>

		        	</div>
		        	<div className="col-md-4">
			        	<div><label>Activity: </label></div>
		        	</div>
		        </div>
		        <div className="modal-footer">
		          <button type="button" className="btn btn-default" onClick={this.closeModal}>Close</button>
		        </div>
        </Modal>

	      <a href="#" onClick={this.openModal}>
		      <div className="item-task">
		      	{this.state.data.title}
	    			<span onClick={this.removeTask.bind(this)} className="button-remove"><i className="fa fa-close"></i></span>
		      </div>
	      </a>
	    </div>
    );
  }
}