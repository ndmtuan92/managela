import React, { Component } from 'react';
import { EditableTextField } from 'react-bootstrap-xeditable';
import ReactDOM from 'react-dom';
import { createContainer } from 'meteor/react-meteor-data';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import { Lists, Tasks } from '../api/collections.js';
import Task from './Task';

import { Meteor } from 'meteor/meteor';

// using some little inline style helpers to make the app look okay
const getItemStyle = (draggableStyle, isDragging) => ({
  // change background colour if dragging
  opacity: isDragging ? '.7' : '1',
  // styles we need to apply on draggables
  ...draggableStyle,
});

// Task component - represents a single todo item
class List extends Component {
	constructor(props) {
    super(props);
    this.state = {
      tasks: this.props.tasks,
      data : this.props.data,
    };
    this.onDragEnd = this.onDragEnd.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
  }
  componentWillReceiveProps(newProps) {
	  if (this.state !== newProps) {
	    this.setState(newProps);
	  }
	}
	handleUpdate(name, value){
  	let data = {};
  	data[name] = value;
  	Lists.update(
    	{ _id: this.state.data._id },
    	{
    		$set : data
    	}
    )
  }
	// a little function to help us with reordering the result
	reorder(list, startIndex, endIndex){
	  const result = Array.from(list);
	  const [removed] = result.splice(startIndex, 1);
	  result.splice(endIndex, 0, removed);
    this.setState({tasks: result});
	}
  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }
    this.reorder(
      this.state.tasks,
      result.source.index,
      result.destination.index
    );
    //update sort all
    this.state.tasks.forEach(function(item, key){
    	Tasks.update(
    		{ _id: item._id },
    		{ $set: { pos: key }
    		}
    	)
    })
  }
	handleSubmit(event){
		event.preventDefault();
		const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
		Tasks.insert({
			title: text,
			listId: this.props.data._id,
			pos: this.props.tasks.length,
			createAt: new Date(),
		})
		ReactDOM.findDOMNode(this.refs.textInput).value = '';
	}
	removeList(){
		Meteor.call('removeList', this.props.data._id);
	}
  render() {
    return (
      <div className="col-md-3">
      	<div className="item-list">
      		<div className="item-title">
      			<EditableTextField 
          		name='title'
          		value={this.state.data.title}
          		onUpdate={this.handleUpdate} 
          		placeholder='Please input title'
          	/>
      			<span onClick={this.removeList.bind(this)} className="button-remove"><i className="fa fa-close"></i></span>
      		</div>
      		<div className="clearfix" />
      		<hr />
      		<form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
	          <div className="form-group">
	            <input
	            	required
	              type="text"
	              ref="textInput"
	              placeholder="Type to add new task"
	              className="form-control"
	            />
	          </div>
	        </form>
	        {/*
	        <DragDropContext onDragEnd={this.onDragEnd.bind(this)} >
		        <Droppable droppableId="droppable">
		          {(provided, snapshot) => (
		            <div
		              ref={provided.innerRef}
		            >
		              {this.state.tasks.map(item => (
		                <Draggable key={item._id} draggableId={item._id}>
		                  {(provided, snapshot) => (
		                    <div>
		                      <div
		                        ref={provided.innerRef}
		                        style={getItemStyle(
		                          provided.draggableStyle,
		                          snapshot.isDragging
		                        )}
		                        {...provided.dragHandleProps}
		                      >
		                        <Task parent={this.props.data} data={item} />
		                      </div>
		                      {provided.placeholder}
		                    </div>
		                  )}
		                </Draggable>
		              ))}
		              {provided.placeholder}
		            </div>
		          )}
		        </Droppable>
		      </DragDropContext>
		    	*/}
		    	{this.state.tasks.map(item => (
            <Task key={item._id} parent={this.props.data} data={item} />
          ))}
      	</div>
      </div>
    );
  }
}

export default createContainer( (props) => {
	return {
  	tasks: Tasks.find({listId : props.data._id}, {sort: {pos: 1, createAt: -1}}).fetch(),
  };
}, List)