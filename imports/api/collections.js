import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { Mongo } from 'meteor/mongo';
 
export const Boards = new Mongo.Collection('boards');
export const Lists = new Mongo.Collection('lists');
export const Tasks = new Mongo.Collection('tasks');

Meteor.methods({
  'removeList' (id) {
    Tasks.remove({listId: id});
		Lists.remove(id);
  }
});